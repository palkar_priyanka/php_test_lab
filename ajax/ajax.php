<?php
/*
	AJAX(Asynchronous javascript and xml)
	javascript
	JSON
	how to put data from ajax result to html
*/
?>
<html>
	<body>
		<input type="text" id="number" name="number" placeholder="Enter number"/>
		<button  onclick="getSquare()">Get square</button>
		<div id="result">
		</div>
	</body>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script>
		function getSquare(){
			$.ajax({
				url:"ajax_result.php",
				method:"POST",
			data : {'number' : $('#number').val()},
			success: function(data){
				console.log("successfully run ajax request..." + data);
				$('#result').html("Square of" + " " + $('#number').val() + " = " + data);
			}
			}).fail(function(){
				console.log("I am from fail function");
			}).always(function(){
				console.log("i am from always function");
			});
		}
		$(document).ready(function(){
			console.log("Hello World i am internal function");
		});
	</script>
</html>