<?php

$students_data = [
    ["id" => 4, "name" => "Shailesh", "age" => 29],
    ["id" => 5, "name" => "Mahesh", "age" => 27],
    ["id" => 6, "name" => "Shilpa", "age" => 25]
];

echo json_encode($students_data);

?>