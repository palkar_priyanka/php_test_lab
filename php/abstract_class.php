<?php

	abstract class Bank{
		
		public function deposit(){
			
			echo "I am from class Bank deposit method";
		}
		
		abstract public function withdraw();
	}
	
	class SBI extends Bank{
		
		public function withdraw(){
			
			echo "</br>I am from SBI withdraw function";
		}
	}
	
	$sbi_obj = new SBI();
	$sbi_obj->deposit();
	$sbi_obj->withdraw();

?>