<!Doctype html>
<html>
	<body>
		<?php
			echo "My First PHP Scripts";
		?>
		<h4> Hello World... </h4>
		
		<?php	
			// case sensitivity
			ECHO "HEllo World</br>";
			Echo "HEllo World</br>";
			EcHO "HEllo World</br>";
			
			// all variable names are case-sensitive.
			
			$color="Red";
			
			echo "My Car is " . $color ."<Br>";
			//echo "My house is" . $COLOR ."<Br>";
			//echo "My boat is" . $coLOR ."<Br>";
			
			// array in php
			
			$color = array("RED","GREEN","BLUE");
			
			$fruits = ["Oranges", "Banana", "Mangoes", "Grapes"];
			
			echo "<pre>";
			
			// The HTML <pre> tag is used for indicating preformatted text. 
			
			var_dump($fruits);
			
			// The PHP var_dump() function returns the data type and value
			
			print_r($fruits);
			
			echo "</pre>";
			
			echo "I LIKE " .$color[0];
		?>
		
	</body>
</html>