<html>
    <head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    </head>
    <body>
        <form>
            Search here : <input type="text" name="search" id="search" onkeyup="showResult()">
        </form>
        <div id="mytable">
        </div>
        <script>
            /*function showResult(str){
                if(str.length == 0){
                    document.getElementById("result").innerHTML = "";
                    return;
                }else{
                    var xmlhttp = new XMLHttpRequest();
                    xmlhttp.onreadystatechange = function() {
                        if(this.readyState == 4 && this.status == 200){
                            document.getElementById("result").innerHTML = this.responseText;
                        }
                    };
                    xmlhttp.open("GET","resultdata.php?q=" + str,true);
                    xmlhttp.send();
                }
            }*/
            function showResult(){
                $.ajax({
                    url : "resultdata.php",
                    method : "POST",
                    data : { search : $('#search').val()},
                    success : function (data){

                        data = JSON.parse(data);
                        var tbl=$("<table/>").attr("id","mytable");
                        table_data = "";
                        $("#div1").append(tbl);
                        for(var i=0;i<data.length;i++) {
                            var tr="<tr>";
                            var td1="<td>"+data[i]["user_id"]+"</td>";
                            var td2="<td>"+data[i]["firstname"]+"</td>";
                            var td3="<td>"+data[i]["lastname"]+"</td></tr>";
                            table_data += tr+td1+td2+td3;
                        }   

                        $("#mytable").html(table_data); 
                        
                    }
                });
            }
        </script>
    </body>
</html>