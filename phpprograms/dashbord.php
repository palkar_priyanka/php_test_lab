<!DOCTYPE html>
<html>
	
		<?php
			$servername = "localhost";
			$username = "root";
			$password = "";
			$dbname = "cims";

			// Create connection
			$conn = new mysqli($servername, $username, $password, $dbname);
			// Check connection
			if ($conn->connect_error) {
				die("Connection failed: " . $conn->connect_error);
			} 

			$sql = "SELECT * from students";
			$result = $conn->query($sql);
         ?>
	<body>
		students list
		<table border="1" style="width:100%">
		  <tr>
			<th>ID</th>
			<th>Firstname</th>
			<th>Lastname</th> 
			<th>Age</th>
		  </tr>
		  <?php
			
			if ($result->num_rows > 0) {
				// output data of each row
				while($row = $result->fetch_assoc()) {
			?>
				<tr>
					<td align="center"><?php echo $row["id"]; ?></td>
					<td align="center"><?php echo $row["firstname"]; ?></td> 
					<td align="center"><?php echo $row["lastname"]; ?></td>
					<td align="center"><?php echo $row["age"]; ?></td>
				</tr>
			<?php
					//echo "id: " . $row["id"]. " - Name: " . $row["firstname"]. " " . $row["lastname"]. "<br>";
				}
			} else {
				echo "0 results";
			}
			$conn->close();
		  
		  ?>  
		</table>
	</body>
</html>