<?php
	
	$l = isset($_REQUEST['length']) ? $_REQUEST['length'] : 1;
	
	$b = isset($_REQUEST['breadth']) ? $_REQUEST['breadth'] : 1;
	
	
	function areaOfrectangle($length, $breadth)
	{
		$area = $length * $breadth;
		
		return $area;
	}
	$area = areaOfrectangle($l, $b);
	
	function perimetreOfrectangle($length, $breadth)
	{
		$perimetre = 2 * $length * $breadth;
		
		return $perimetre;
	}
	
	$peri = perimetreOfrectangle($l, $b);
?>
<html>
	<body>
		<center>
			<form method="GET">
				<table border="1">
					<caption>Rectangle</caption>
					<tr>
						<td>Length</td>
						<td><input type ="text" name="length" value="<?php echo $l; ?>"/></td>
					</tr>
					<tr>
						<td>Breadth</td>
						<td><input type ="text" name="breadth" value="<?php echo $b; ?>"/></td>
					</tr>
					<tr>
						<td>Area</td>
						<td><b><?php echo $area; ?></b></td>
					</tr>
					<tr>
						<td>Perimeter</td>
						<td><?php echo $peri; ?></td>
					</tr>
					<tr>
						<td><input type="submit"/></td>
						<td><input type="reset"/</td>
					</tr>
				</table>		
			</form>
		</center>
	</body>
</html>