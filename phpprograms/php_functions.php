<?php
	
	/*
	A user defined function declaration starts with the word "function":

Syntax
function functionName() {
    code to be executed;
}
*/

/*
	function WriteMsg(){
		echo "Hello World !!! ";
	}
	WriteMsg(); // call the function
*/
	
	// PHP Function Arguments
	
/*
	function familyname($fname){
		echo "$fname palkar . <br>";
	}
	familyname("priyanka");
	familyname("neha");
	familyname("kunal");
	familyname("Anuradha");
	familyname("Vijay");
*/
	
/*	function familyname($fname,$year){
		echo "$fname palkar born in $year <br>";
	}
	familyname("priyanka","1992");
	familyname("neha","1995");
	familyname("kunal","1989");
*/
	// PHP Functions - Returning values
	
	function sum($x,$y){
		$z = $x + $y;
		return $z;
	}
	echo " 5+10= " . sum(5,10) . "<br>";
	echo " 6+6 = " . sum(6,6) . "<br>";
	echo " 2+4 = " . sum(2,4);
?>