<?php
	$cookie_name = "user";
	$cookie_value = "John Doe";
	setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/");
	setcookie("mobile_number", "9082966101", time() + (86400 * 30), "/");
?>

<!DOCTYPE html>
<html>
	<body>
		<fieldset>
			<legend>Personal Information</legend>
			<form action="task_action.php" method="GET">
				<table border="1">
					<tr>
						<td>First Name</td>
						<td><input type="text" name="firstname" placeholder="Enter your firstname"/></td>
					</tr>
					<tr>
						<td>Middle Name</td>
						<td><input type="text" name="middlename" placeholder="Enter your middlename"/></td>
					</tr>
					<tr>
						<td>Last Name</td>
						<td><input type="text" name="lastname" placeholder="Enter your lastname"/>
					</tr>
					<tr>
						<td>Contact Number</td>
						<td><input type="text" name="contact" placeholder="Enter your contact number"/>
					</tr>
					<tr>
						<td>Address</td>
						<td><textarea name="address" rows="3" placeholder="Enter your complete address"></textarea></td>
					</tr>
					<tr>
						<td>Gender</td>
						<td><input type="radio" name="gender" value="Male">male <br>
						<input type="radio" name="gender" value="Female">Female</td>
					</tr>
					<tr>
						<td colspan="2" align="center"><input type="submit" value="submit"></td>
					</tr>
				</table>
			</form>
		</fieldset>	
	</body>
</html>